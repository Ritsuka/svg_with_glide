package wachi.survey.svgbyglide

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val httpUrl =  "http://www.clker.com/cliparts/u/Z/2/b/a/6/android-toy-h.svg"
    private val httpsUrl =  "https://upload.wikimedia.org/wikipedia/commons/0/09/America_Online_logo.svg"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Glide.with(this).load(httpUrl).into(img_http)
        Glide.with(this).load(httpsUrl).into(img_https)
    }
}
